# Contents of this file

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers


# Introduction

This module provides a basic setup for a Totara (Moodle) intergration.
It includes:
* a config form for storing the Totara url and the webservice token
* 2 new events:
  - totara.new_user_data_for_totara_prepare_event
  - totara.user_data_for_totara_prepare_event
* a Totara client service and interface
* a Totara user data service and interface
* a totara_auth_userkey submodule


# Requirements

A Totara instance is required, but this module requires no modules outside of Drupal core.


# Installation

Install as you would normally install a contributed Drupal module. Visit https://www.drupal.org/node/1897420 for further information.


# Configuration

There is some configuration necessary in Drupal and in Totara.

## Drupal setup

In the module configuration, add:
* The URL of the Totara instance.
* The webservice token, as created before in Totara.

## Totara setup

The steps below are based on the overview as provided on the `Site Administration
/ Plugins / Web services / Overview` page in Totara.

* Enable web services
* Enable the 'REST protocol'

* Create a user for the Drupal Totara Client, that we will configure with the
  minimal permissions to do its job.

* Create a new role 'webservices', with following permissions:
  * moodle/course:useremail
  * moodle/site:viewuseridentity
  * moodle/user:create
  * moodle/user:delete
  * moodle/user:update
  * moodle/user:viewdetails
  * moodle/user:viewhiddendetails
  * moodle/webservice:createtoken
  * webservice/rest:use

* Assign the role 'webservice' role to the user.

* Create a custom Service with following options:
  * Enabled: TRUE
  * Authorised users only: TRUE
  * Required capability:
    * Add at least "webservice/rest:use: Use REST protocol"

* After saving the Service, add at least the following functions to the Service:
  * To check Totara availability: core_webservice_get_site_info
  * For User actions and checks:
    * core_user_create_users
    * core_user_get_users_by_field
    * core_user_update_users
    * core_user_delete_users

* Create a token for the user - Service combination. This token needs to be added in Drupal.

## Tokens

Provides token for:

* Totara URL.


# Maintainers

Current maintainers:
* Andreas De Rijcke (andreasderijcke) - http://drupal.org/user/3260992
* Stefanie Vermandele (StefanieV) - http://drupal.org/user/3591769

This project has been sponsored by:
* CALIBRATE: We are a full-service Drupal service provider based out of Europe. 
  We help our clients with digital strategic planning, UX and UI design and testing, development, and digital marketing services. 
  As a data driven strategic partner, we use facts, research and KPI's to achieve better results.
  Want to learn more about Calibrate? Visit www.calibrate.be.
