<?php

namespace Drupal\totara_auth_userkey;

use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the Totara Auth Userkey service.
 */
interface TotaraAuthUserkeyInterface {

  /**
   * The config key of the module settings.
   *
   * @var string
   */
  const CONFIG_KEY = 'totara_auth_userkey.settings';

  /**
   * The config key of the 'allowed roles' value.
   *
   * @var string
   */
  const CONFIG_VALUE_ALLOWED_ROLES = 'allowed_roles';

  /**
   * The config key of the token value.
   *
   * @var string
   */
  const CONFIG_VALUE_TOKEN = 'token';

  /**
   * The config key of the default wantsurl value.
   *
   * @var string
   */
  const CONFIG_VALUE_WANTSURL = 'wantsurl';

  /**
   * Get the allowed roles for SSO.
   *
   * @return array
   *   The Role IDs.
   */
  public function allowedRoles(): array;

  /**
   * Check if given account is allowed to SSO.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account.
   *
   * @return bool
   *   TRUE if the account has at least one allowed role.
   */
  public function allowedToSso(AccountInterface $account): bool;

  /**
   * SSO login and redirect the current User to Totara.
   *
   * @param string|null $wants_url
   *   The (relative) path or a full url to redirect the User to after SSO into
   *   Totara.
   *
   * @return \Drupal\Core\Routing\TrustedRedirectResponse
   *   The redirect.
   */
  public function login(string $wants_url = NULL): TrustedRedirectResponse;

  /**
   * Get the logout redirect for current user for Totara.
   *
   * @param string $route_name
   *   The redirect route to pass to Totara for redirection after the logout in
   *   Totara.
   * @param array $route_parameters
   *   Parameters for the route.
   *
   * @return \Drupal\Core\Routing\TrustedRedirectResponse|null
   *   The redirect to Totara logout URL.
   */
  public function logout(string $route_name, array $route_parameters = []): ?TrustedRedirectResponse;

  /**
   * Logout given account.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account.
   */
  public function logoutUser(AccountInterface $account): void;

}
