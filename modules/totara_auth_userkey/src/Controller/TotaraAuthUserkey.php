<?php

namespace Drupal\totara_auth_userkey\Controller;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\totara_auth_userkey\TotaraAuthUserkeyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Totara Auth Userkey routes.
 */
class TotaraAuthUserkey extends ControllerBase {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The Totara Auth Userkey service.
   *
   * @var \Drupal\totara_auth_userkey\TotaraAuthUserkeyInterface
   */
  protected $totaraAuthUserkey;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\totara_auth_userkey\TotaraAuthUserkeyInterface $totara_auth_userkey
   *   The Totara Auth Userkey service.
   */
  public function __construct(
    AccountProxyInterface $current_user,
    RequestStack $request_stack,
    TotaraAuthUserkeyInterface $totara_auth_userkey
  ) {
    $this->currentUser = $current_user;
    $this->request = $request_stack->getCurrentRequest();
    $this->totaraAuthUserkey = $totara_auth_userkey;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('request_stack'),
      $container->get('totara.auth_userkey')
    );
  }

  /**
   * Forward a request to Totara, with SSO for logged-in Users.
   *
   * Takes 'wantsurl' query parameters into account, which can be a (relative)
   * path or a full url to redirect the User to after SSO into Totara.
   *
   * @return \Drupal\Core\Routing\TrustedRedirectResponse
   *   The redirect.
   */
  public function forward(): TrustedRedirectResponse {

    $wants_url = $this->getWantsUrl();

    // Just forward anonymous request to Totara, using the path in 'wantsurl'
    // (if set) as target path.
    if ($this->currentUser->isAnonymous()) {

      /** @var \Drupal\totara\TotaraClientInterface $totara_client */
      $totara_client = \Drupal::service('totara.client');
      $url = $totara_client->getBaseUrl();

      if (!is_null($wants_url)) {
        $url .= $wants_url;
      }

      $response = new TrustedRedirectResponse($url);
      $cacheable_metadata = new CacheableMetadata();

      $cache_contexts = ['user.roles:anonymous', 'url.query_args'];
      $cacheable_metadata->addCacheContexts($cache_contexts);
      $cacheable_metadata->addCacheContexts($totara_client->getCacheContexts());
      $cacheable_metadata->addCacheTags($totara_client->getCacheTags());
      $cacheable_metadata->mergeCacheMaxAge($totara_client->getCacheMaxAge());
      $response->addCacheableDependency($cacheable_metadata);

      return $response;
    }

    // Redirect and force any cache metadata to bubble now, or we get trouble
    // in early rendering.
    $url = $this->getDrupalSsoUrl($wants_url);
    $url = $url->toString(TRUE)->getGeneratedUrl();

    // Don't cache this response.
    $response = new TrustedRedirectResponse($url);
    $cacheable_metadata = new CacheableMetadata();
    $cacheable_metadata->setCacheMaxAge(0);
    $response->addCacheableDependency($cacheable_metadata);

    return $response;
  }

  /**
   * Return redirect to Totara SSO URL or to front if not allowed.
   *
   * Takes 'wantsurl' query parameters into account, which can be a (relative)
   * path or a full url to redirect the User to after SSO into Totara.
   *
   * @return \Drupal\Core\Routing\TrustedRedirectResponse
   *   The redirect.
   */
  public function login(): TrustedRedirectResponse {
    $wants_url = $this->request->query->get('wantsurl');
    return $this->totaraAuthUserkey->login($wants_url);
  }

  /**
   * Handle incoming login request from Totara with Drupal login and Totara SSO.
   *
   * Takes 'wantsurl' query parameters into account, which can be a (relative)
   * path or a full url to redirect the User to after SSO into Totara.
   *
   * @return \Drupal\Core\Routing\TrustedRedirectResponse
   *   The redirect.
   */
  public function loginIncoming(): TrustedRedirectResponse {

    $url = $this->getDrupalSsoUrl($this->getWantsUrl());

    // Anonymous must authenticate in Drupal first, obviously, so we add the
    // Drupal SSO url as destination for redirection after login.
    if ($this->currentUser->isAnonymous()) {

      $options = [
        'query' => [
          'destination' => $url->toString(TRUE)->getGeneratedUrl(),
        ],
      ];

      $url = Url::fromRoute('user.login', [], $options);
    }

    // Redirect and force any cache metadata to bubble now, or we get trouble
    // in early rendering.
    $url = $url->toString(TRUE)->getGeneratedUrl();

    $response = new TrustedRedirectResponse($url);
    $cacheable_metadata = new CacheableMetadata();

    if ($this->currentUser->isAnonymous()) {
      $cache_contexts = ['user.roles:anonymous', 'url.query_args'];
      $cacheable_metadata->addCacheContexts($cache_contexts);
    }
    else {
      // Don't cache for authenticated Users, until we're sure this is okay
      // with the consecutive redirects.
      $cacheable_metadata->setCacheMaxAge(0);
    }

    $response->addCacheableDependency($cacheable_metadata);

    return $response;
  }

  /**
   * Get URL of the internal SSO route (Login - Outgoing).
   *
   * @param string|null $wants_url
   *   The (relative) path or a full url to redirect the User to after SSO into
   *   Totara.
   *
   * @return \Drupal\Core\Url
   *   The Url.
   */
  protected function getDrupalSsoUrl(string $wants_url = NULL) {

    $options = [];

    // Pass on the 'wantsurl' for redirect after SSO in Totara.
    if (!is_null($wants_url)) {
      $options['query'] = [
        'wantsurl' => $wants_url,
      ];
    }

    return Url::fromRoute('totara_auth_userkey.login', [], $options);
  }

  /**
   * Get the cleaned 'wantsurl' query parameter.
   *
   * @return string|null
   *   The cleaned 'wantsurl' for redirect after SSO in Totara.
   */
  protected function getWantsUrl(): ?string {

    $wants_url = $this->request->query->get('wantsurl');
    if (!is_null($wants_url)) {

      // As we want to enforce forward to Totara, remove all url parts but the
      // path, query and fragment.
      $url_parts = parse_url($wants_url);
      if (isset($url_parts['path'])) {

        $wants_url = $url_parts['path'];
        if (substr($wants_url, 0, 1) !== '/') {
          $wants_url = "/$wants_url";
        }

        if (isset($url_parts['query'])) {
          $wants_url = "$wants_url?{$url_parts['query']}";
        }

        if (isset($url_parts['fragment'])) {
          $wants_url = "$wants_url#{$url_parts['fragment']}";
        }
      }
      else {
        $wants_url = NULL;
      }
    }

    return $wants_url;
  }

}
