<?php

namespace Drupal\totara_auth_userkey\EventSubscriber;

use Drupal\totara\Event\NewUserDataForTotaraPrepareEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Responds to User data related events from Totara module.
 */
class TotaraAuthUserkeyUserDataForTotaraEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      NewUserDataForTotaraPrepareEvent::EVENT_NAME => 'addDataForNewUser',
    ];
  }

  /**
   * Add additional User data for a new user in Totara.
   *
   * @param \Drupal\totara\Event\NewUserDataForTotaraPrepareEvent $event
   *   The aevent.
   */
  public function addDataForNewUser(NewUserDataForTotaraPrepareEvent $event) {

    $data = $event->getData();

    // Set the authentication method to our plugin, and make sure no password
    // is generated and mailed as it does not matter.
    $data['auth'] = 'userkey';
    $data['createpassword'] = 0;

    $event->setData($data);
  }

}
