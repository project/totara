<?php

namespace Drupal\totara_auth_userkey\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\totara_auth_userkey\TotaraAuthUserkeyInterface;

/**
 * Checks if a User is allowed to use the SSO.
 */
class TotaraAuthUserkeySSOAccessCheck implements AccessInterface {

  /**
   * The Totara Auth Userkey service.
   *
   * @var \Drupal\totara_auth_userkey\TotaraAuthUserkeyInterface
   */
  protected $totaraAuthUserkey;

  /**
   * Constructor.
   *
   * @param \Drupal\totara_auth_userkey\TotaraAuthUserkeyInterface $totara_auth_userkey
   *   The Totara Auth Userkey service.
   */
  public function __construct(TotaraAuthUserkeyInterface $totara_auth_userkey) {
    $this->totaraAuthUserkey = $totara_auth_userkey;
  }

  /**
   * Checks access for SSO.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account) {

    if ($account->isAnonymous()) {
      // Anonymous users are never allowed to access.
      return AccessResult::forbidden()->addCacheContexts(['user.roles:authenticated']);
    }

    if ($this->totaraAuthUserkey->allowedToSso($account)) {
      return AccessResult::allowed()->addCacheableDependency($account);
    }

    return AccessResult::forbidden()->addCacheableDependency($account);
  }

}
