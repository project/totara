<?php

namespace Drupal\totara_auth_userkey\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\totara_auth_userkey\TotaraAuthUserkeyInterface;
use Drupal\user\Entity\Role;

/**
 * Totara Auth Userkey config form.
 */
class TotaraAuthUserkeyConfigForm extends ConfigFormBase {

  /**
   * The Totara Auth Userkey config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      TotaraAuthUserkeyInterface::CONFIG_KEY,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'totara_auth_userkey_settings_form';
  }

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);

    $config_keys = $this->getEditableConfigNames();
    $this->config = $this->config(reset($config_keys));
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $form[TotaraAuthUserkeyInterface::CONFIG_VALUE_TOKEN] = [
      '#type' => 'textfield',
      '#title' => $this->t('Webservice token'),
      '#description' => $this->t('The webservice token, as configured in Totara for the "User key authentication web service" Service.'),
      '#maxlength' => 255,
      '#required' => TRUE,
      '#default_value' => $this->config->get(TotaraAuthUserkeyInterface::CONFIG_VALUE_TOKEN),
    ];


    $form[TotaraAuthUserkeyInterface::CONFIG_VALUE_WANTSURL] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default value for wantsurl parameter.'),
      '#description' => $this->t('The wantsurl parameter can be used to redirect to a certain page after SSO. In case guest login is enabled in Totara, this is required to avoid interference with the SSO. We suggest using "/totara/dashboard/index.php".'),
      '#maxlength' => 255,
      '#required' => FALSE,
      '#default_value' => $this->config->get(TotaraAuthUserkeyInterface::CONFIG_VALUE_WANTSURL),
    ];

    $roles = Role::loadMultiple();
    unset($roles[AccountInterface::ANONYMOUS_ROLE]);
    foreach ($roles as $id => $role) {
      $roles[$id] = $role->label();
    }

    $form[TotaraAuthUserkeyInterface::CONFIG_VALUE_ALLOWED_ROLES] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Allowed Roles for SSO'),
      '#description' => $this->t('Roles that are allowed to SSO into Totara. For every Drupal User trying to SSO into Totara, a user will be created in Totara if they do not have one yet.'),
      '#required' => TRUE,
      '#options' => $roles,
      '#default_value' => $this->config->get(TotaraAuthUserkeyInterface::CONFIG_VALUE_ALLOWED_ROLES) ?? [],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $form_state->cleanValues();
    $this->config->setData($form_state->getValues())->save();

    parent::submitForm($form, $form_state);
  }

}
