<?php

namespace Drupal\totara_auth_userkey;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Logger\LoggerChannel;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\totara\Exceptions\TotaraRequestInvalidResponseException;
use Drupal\totara\Exceptions\TotaraUnreachableException;
use Drupal\totara\Exceptions\TotaraWsTokenException;
use Drupal\totara\TotaraClientInterface;
use Drupal\user\Entity\User;

/**
 * Default implementation of the Totara Auth Userkey service.
 */
class TotaraAuthUserkey implements TotaraAuthUserkeyInterface {

  use StringTranslationTrait;

  /**
   * The module config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The current user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $currentUser;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The WS token.
   *
   * @var string
   */
  protected $token;

  /**
   * The Totara Client.
   *
   * @var \Drupal\totara\TotaraClientInterface
   */
  protected $totaraClient;

  /**
   * The default wantsurl.
   *
   * @var string
   */
  protected $wantsurl;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Logger\LoggerChannel $logger
   *   The logger interface.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\totara\TotaraClientInterface $totara_client
   *   The Totara Client.
   */
  public function __construct(
    ConfigFactory $config_factory,
    AccountProxyInterface $current_user,
    LoggerChannel $logger,
    MessengerInterface $messenger,
    TotaraClientInterface $totara_client
  ) {

    $this->config = $config_factory->get(static::CONFIG_KEY);
    $this->currentUser = User::load($current_user->id());
    $this->logger = $logger;
    $this->messenger = $messenger;
    $this->token = $this->config->get(static::CONFIG_VALUE_TOKEN);
    $this->totaraClient = $totara_client;
    $this->wantsurl = $this->config->get(static::CONFIG_VALUE_WANTSURL);
  }

  /**
   * {@inheritdoc}
   */
  public function allowedRoles(): array {

    $roles = $this->config->get(static::CONFIG_VALUE_ALLOWED_ROLES) ?? [];
    $roles = array_flip($roles);
    unset($roles[0]);

    return array_values($roles);
  }

  /**
   * {@inheritdoc}
   */
  public function allowedToSso(AccountInterface $account): bool {

    $allowed_roles = $this->allowedRoles();
    $roles = $account->getRoles();

    $has_allowed_role = array_intersect($allowed_roles, $roles);
    return !empty($has_allowed_role);
  }

  /**
   * {@inheritdoc}
   */
  public function login(string $wants_url = NULL): TrustedRedirectResponse {

    $url = NULL;

    try {
      $this->basicApiChecks();

      if (!$this->totaraClient->checkUserExists($this->currentUser)) {
        $this->messenger->addError($this->t('Something when wrong trying to create your account in Totara.'));
        throw new \Exception("Unable to create Totara user for Drupal User with ID {$this->currentUser->id()}");
      }

      $params['user']['email'] = $this->currentUser->getEmail();
      $response = $this->totaraClient->post('auth_userkey_request_login_url', $params, $this->token);

      if (!isset($response['loginurl']) || empty($response['loginurl'])) {
        $this->messenger->addError($this->t('Something when wrong trying to log you in into Totara.'));
        throw new TotaraRequestInvalidResponseException('Missing loginurl');
      }

      if (is_null($wants_url) && !empty($this->wantsurl)) {
        $wants_url = $this->wantsurl;
      }

      $url = $response['loginurl'];
      if (!is_null($wants_url)) {
        $url = "$url&wantsurl=" . urlencode($wants_url);
      }
    }
    catch (TotaraUnreachableException | TotaraWsTokenException $exception) {
      $this->logger->error('{error}', ['error' => $exception->getMessage()]);
      $this->messenger->addError($this->t('Totara is currently unavailable. Please try again later.'));
    }
    catch (\Exception $exception) {
      $this->logger->error('{error}', ['error' => $exception->getMessage()]);
    }

    if (is_null($url)) {

      // @todo Update url to referrer, if present.
      // Redirect and force any cache metadata to bubble now, or we get trouble
      // in early rendering.
      $url = Url::fromRoute('<front>')->toString(TRUE)->getGeneratedUrl();
    }

    // Don't cache this response.
    $response = new TrustedRedirectResponse($url);
    $cacheable_metadata = new CacheableMetadata();
    $cacheable_metadata->setCacheMaxAge(0);
    $response->addCacheableDependency($cacheable_metadata);

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function logout(string $route_name, array $route_parameters = []): ?TrustedRedirectResponse {

    if (!$this->totaraClient->ping()) {
      // @todo defer to queue.
      // @todo we can only do something usefull in queue if the auth_userkey
      // plugin provides logout using webservice. Their only option they offer
      // now, would require use to override UserController and its logout
      // function, @see \Drupal\user\Controller\UserController::logout, which we
      // rather not do.
      return NULL;
    }

    // Redirect and force any cache metadata to bubble now, or we get trouble
    // in early rendering.
    $return_url = Url::fromRoute($route_name, $route_parameters, ['absolute' => TRUE])->toString(TRUE)->getGeneratedUrl();

    $url = "{$this->totaraClient->getBaseUrl()}/auth/userkey/logout.php?return=$return_url";

    // Don't cache this response.
    $response = new TrustedRedirectResponse($url);
    $cacheable_metadata = new CacheableMetadata();
    $cacheable_metadata->setCacheMaxAge(0);
    $response->addCacheableDependency($cacheable_metadata);

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function logoutUser(AccountInterface $account): void {

    if (!$this->totaraClient->ping()) {
      // @todo defer to queue.
      return;
    }

    try {

      $params['user']['email'] = $account->getEmail();
      $this->totaraClient->post('auth_userkey_logout_user', $params, $this->token);
    }
    catch (\Exception $exception) {
      $context = [
        'email' => $account->getEmail(),
        'uid' => $account->id(),
        'exception' => $exception->getMessage(),
      ];
      $this->logger->error('Failed to logout user {email}({uid}): {exception}', $context);
    }
  }

  /*
   * Helpers.
   */

  /**
   * Basic API/connection checks.
   *
   * @throws \Drupal\totara\Exceptions\TotaraUnreachableException
   * @throws \Drupal\totara\Exceptions\TotaraWsTokenException
   */
  protected function basicApiChecks(): void {

    if (empty($this->token)) {
      throw new TotaraWsTokenException('Totara Auth Userkey WS token is missing.');
    }

    if (!$this->totaraClient->ping()) {
      throw new TotaraUnreachableException('Totara is currently unavailable.');
    }
  }

}
