# Contents of this file

* Introduction
* Requirements
* Configuration


# Introduction

@todo

# Requirements

The Totara setup requires:
* the [Auth Userkey plugin](https://moodle.org/plugins/auth_userkey).
* the plugin on [Github](https://github.com/catalyst/moodle-auth_userkey).

# Installation

The totara module should be enabled and configured to enable and use this submodule.

# Configuration

## Totara setup

* Set the plugin to use `email` as mapping field in Totara.
* Add the user created for the main service also as authorised user to
  the 'User key authentication web service' Service.
* Create a token for the user - 'User key authentication web service' Service
  combination. This token needs to be added in Drupal.

Warning

If you already have users in Totara, you will need to update their 'auth' method
to 'userkey', via database manipulation, or they won't be able to get a SSO login
URL!


## Drupal setup

In the module configuration, add:

* The webservice token, as created before in Totara.
* Roles that are allowed to SSO into Totara.


## Routes

 * Forward (`/totara/forward`)
 * Login - Outgoing (`/totara/login/outgoing`)
 * Login - Incoming (`/totara/login/incoming`)


## Tokens

Provides token for:

* Totara SSO URL.
