<?php

namespace Drupal\totara;

use Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Logger\LoggerChannel;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\totara\Event\NewUserDataForTotaraPrepareEvent;
use Drupal\totara\Event\UserDataForTotaraPrepareEvent;
use Drupal\totara\Exceptions\TotaraRequestFailedException;
use Drupal\totara\Exceptions\TotaraRequestInvalidResponseException;
use Drupal\user\UserInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Default implementation of the Totara Client.
 */
class TotaraClient implements TotaraClientInterface {

  use StringTranslationTrait;

  /**
   * The Totara base URL, or domain.
   *
   * @var string
   */
  protected $baseUrl;

  /**
   * The module config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The event dispatcher.
   *
   * @var \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher
   */
  protected $eventDispatcher;

  /**
   * Guzzle HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The Totara webservice URL.
   *
   * This does not include webservice function and token.
   *
   * @var string
   */
  protected $wsUrl;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory.
   * @param \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher $event_dispatcher
   *   The event dispatcher.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The http client.
   * @param \Drupal\Core\Logger\LoggerChannel $logger
   *   The logger interface.
   */
  public function __construct(
    ConfigFactory $config_factory,
    ContainerAwareEventDispatcher $event_dispatcher,
    ClientInterface $http_client,
    LoggerChannel $logger
  ) {
    $this->config = $config_factory->get(static::CONFIG_KEY);
    $this->eventDispatcher = $event_dispatcher;
    $this->httpClient = $http_client;
    $this->logger = $logger;

    // Build the base and ws URL once.
    $this->baseUrl = $this->config->get(static::CONFIG_VALUE_URL) ?? '';
    $this->wsUrl = "$this->baseUrl/webservice/rest/server.php?moodlewsrestformat=json";
  }

  /*
   * Basic request functions.
   */

  /**
   * {@inheritdoc}
   */
  public function get(string $ws_function, array $params = [], string $ws_token = NULL): array {
    return $this->request('GET', $ws_function, $params, $ws_token);
  }

  /**
   * {@inheritdoc}
   */
  public function post(string $ws_function, array $params = [], string $ws_token = NULL): array {
    return $this->request('POST', $ws_function, $params, $ws_token);
  }

  /*
   * Totara webservice specific request functions.
   */

  /**
   * {@inheritdoc}
   */
  public function addUser(UserInterface $user): UserInterface {
    $users = $this->addUsers([$user]);
    return reset($users);
  }

  /**
   * {@inheritdoc}
   */
  public function addUsers(array $users): array {

    // Collect usernames we are sending, to aid in processing the response.
    $usernames = [];

    $data = [];
    /** @var \Drupal\user\UserInterface $user */
    foreach ($users as $user) {

      $user_data = $this->prepareUserDataForTotara($user);
      $user_data['createpassword'] = 1;

      $event = new NewUserDataForTotaraPrepareEvent($user, $user_data);
      $this->eventDispatcher->dispatch($event, NewUserDataForTotaraPrepareEvent::EVENT_NAME);
      $user_data = $event->getData();

      // Remove the Totara ID if present.
      unset($user_data['id']);

      // We need the username to be able to process the response.
      if (!isset($user_data['username'])) {
        $user_data['username'] = $user->getDisplayName();
      }
      $usernames[] = $user_data['username'];

      $data[] = $user_data;
    }

    try {
      $params = [
        'users' => $data,
      ];
      $response = $this->post('core_user_create_users', $params);

      // As the response contains the username and the corresponding Totara ID,
      // all we can do is store the ID in Drupal.
      /** @var \Drupal\totara\TotaraUserDataInterface $totara_user_data */
      $totara_user_data = \Drupal::service('totara.user.data');
      foreach ($response as $item) {

        $user_index = array_search($item['username'], $usernames);
        if ($user_index === FALSE || !isset($users[$user_index])) {
          throw new \Exception("No Drupal User found For Totara {$item['username']} after user create in Totara.");
        }

        $totara_user_data->setTotaraId($users[$user_index]->id(), $item['id']);
        $users[$user_index]->{TotaraUserDataInterface::TOTARA_ID} = $item['id'];
      }
    }
    catch (TotaraRequestFailedException | TotaraRequestInvalidResponseException $exception) {
      // Do nothing, they already have been logged.
    }
    catch (\Exception $exception) {
      $this->logger->error('{error}', ['error' => $exception->getMessage()]);
    }

    return $users;
  }

  /**
   * {@inheritdoc}
   */
  public function checkUserExists(UserInterface $user): bool {

    // Check using its Totara ID (if set) or email.
    $user_data = $this->getUserData($user);

    // Got a response with a Totara, so the User exists, just have to make sure
    // the ID still matches.
    if ($user_data && isset($user_data['id'])) {

      // If we did not have a Totara ID, or we had and it differs (implies the
      // match was found by email as fallback), so we need to update what we
      // have stored.
      // Intentionally loose comparison here!
      if ($user_data['id'] != $user->{TotaraUserDataInterface::TOTARA_ID}) {

        /** @var \Drupal\totara\TotaraUserDataInterface $torara_user_data */
        $torara_user_data = \Drupal::service('totara.user.data');
        $torara_user_data->setTotaraId($user->id(), $user_data['id']);
        $user->{TotaraUserDataInterface::TOTARA_ID} = $user_data['id'];
      }

      return TRUE;

    }
    // No User found, try and create one.
    else {

      $user = $this->addUser($user);
      return !empty($user->{TotaraUserDataInterface::TOTARA_ID});
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteUser(UserInterface $user): UserInterface {
    $users = $this->deleteUsers([$user]);
    return reset($users);
  }

  /**
   * {@inheritdoc}
   *
   * @todo Finish.
   */
  public function deleteUsers(array $users): array {

    $data = [];
    /** @var \Drupal\user\UserInterface $user */
    foreach ($users as $user) {

      if ($user->{TotaraUserDataInterface::TOTARA_ID}) {
        $data[] = $user->{TotaraUserDataInterface::TOTARA_ID};
      }
    }

    try {

      $params = [
        'userids' => $data,
      ];
      $response = $this->post('core_user_delete_users', $params);

      // @todo Remove the Totara ID from the Users.
    }
    catch (TotaraRequestFailedException | TotaraRequestInvalidResponseException $exception) {
      // Do nothing, they already have been logged.
    }
    catch (\Exception $exception) {
      $this->logger->error('{error}', ['error' => $exception->getMessage()]);
    }

    return $users;
  }

  /**
   * {@inheritdoc}
   */
  public function getUserData(UserInterface $user): array {

    if (!empty($user->{TotaraUserDataInterface::TOTARA_ID})) {

      try {

        $params = [
          'field' => 'id',
          'values' => [$user->{TotaraUserDataInterface::TOTARA_ID}],
        ];
        $data = $this->post('core_user_get_users_by_field', $params);
        // The service returns a list of user data. There can be only one match,
        // in this case.
        $data = reset($data);

        // If the Totara ID is there, we're done. If not, consider it a fail and
        // continue to try and find a match by e-mail.
        if (isset($data['id'])) {
          return $data;
        }
      }
      catch (TotaraRequestFailedException | TotaraRequestInvalidResponseException $exception) {
        // Do nothing, they already have been logged.
      }
      catch (\Exception $exception) {
        $this->logger->error('{error}', ['error' => $exception->getMessage()]);
      }
    }

    try {

      $params = [
        'field' => 'email',
        'values' => [$user->getEmail()],
      ];
      $data = $this->post('core_user_get_users_by_field', $params);

      // The service returns a list of user data. There can be only one match,
      // in this case.
      $data = reset($data);

      // If the Totara ID is not there, consider it a fail.
      return isset($data['id']) ? $data : [];
    }
    catch (TotaraRequestFailedException | TotaraRequestInvalidResponseException $exception) {
      // Do nothing, they already have been logged.
    }
    catch (\Exception $exception) {
      $this->logger->error('{error}', ['error' => $exception->getMessage()]);
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function ping(bool $show_errors = FALSE): bool {

    try {

      $body = $this->get('core_webservice_get_site_info');

      if (!isset($body['sitename'])) {
        throw new \Exception('Ping did not respond with the expected data. Are the credentials okay? Has the webservice user still has the appropriate permission in Totara?');
      }

      return TRUE;
    }
    catch (TotaraRequestFailedException | TotaraRequestInvalidResponseException $exception) {
      // Do nothing, they already have been logged.
    }
    catch (\Exception $exception) {
      $this->logger->error('{error}', ['error' => $exception->getMessage()]);

      if ($show_errors) {
        \Drupal::messenger()->addError($this->t('Connection to Totara failed. Check the logs for more information.'));
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   *
   * @todo Finish.
   */
  public function suspendUser(UserInterface $user): void {
    // @todo
  }

  /**
   * {@inheritdoc}
   *
   * @todo Finish.
   */
  public function suspendUsers(array $users): void {
    // @todo
  }

  /**
   * {@inheritdoc}
   */
  public function updateUser(UserInterface $user): void {
    $this->updateUsers([$user]);
  }

  /**
   * {@inheritdoc}
   */
  public function updateUsers(array $users): void {

    $data = [];
    /** @var \Drupal\user\UserInterface $user */
    foreach ($users as $user) {
      $data[] = $this->prepareUserDataForTotara($user);
    }

    try {
      $params = [
        'users' => $data,
      ];
      $this->post('core_user_update_users', $params);
    }
    catch (TotaraRequestFailedException | TotaraRequestInvalidResponseException $exception) {
      // Do nothing, they already have been logged.
    }
    catch (\Exception $exception) {
      $this->logger->error('{error}', ['error' => $exception->getMessage()]);
    }
  }

  /*
   * Cache interface functions, to expose the config cache info without exposing
   * the config data.
   */

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts(): array {
    return $this->config->getCacheContexts();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags(): array {
    return $this->config->getCacheTags();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge(): int {
    return $this->config->getCacheMaxAge();
  }

  /*
   * Public helper functions.
   */

  /**
   * {@inheritdoc}
   */
  public function getBaseUrl(): string {
    return $this->baseUrl;
  }

  /**
   * {@inheritdoc}
   */
  public function getWsUrl() : string {
    return $this->wsUrl;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareUserDataForTotara(UserInterface $user): array {

    $data = [
      'email' => $user->getEmail(),
      // Totara username only allows lower case.
      'username' => strtolower($user->getDisplayName()),
      'firstname' => '',
      'lastname' => '',
      'customfields' => [],
    ];

    if (!empty($user->{TotaraUserDataInterface::TOTARA_ID})) {
      $data['id'] = $user->{TotaraUserDataInterface::TOTARA_ID};
    }

    $event = new UserDataForTotaraPrepareEvent($user, $data);
    $this->eventDispatcher->dispatch($event, UserDataForTotaraPrepareEvent::EVENT_NAME);

    return $event->getData();
  }

  /**
   * {@inheritdoc}
   */
  public static function addCustomField(array &$data, string $type, string $value): void {

    // Make sure the custom field container is still there.
    if (!isset($data['customfields'])) {
      $data['customfields'] = [];
    }

    $data['customfields'][] = [
      'type' => $type,
      'value' => $value,
    ];
  }

  /*
   * Our own helper functions.
   */

  /**
   * Builds webservice url for given webservice function.
   *
   * @param string $ws_function
   *   The Totara webservice function to call.
   * @param string|null $ws_token
   *   Webservice token override. Defaults to NULL, the default of the Totara
   *   Client will be used.
   *
   * @return string
   *   The full webservice url.
   */
  protected function compileUrl(string $ws_function, string $ws_token = NULL) {

    $ws_token = $ws_token ?? ($this->config->get(static::CONFIG_VALUE_TOKEN) ?? '');
    return "$this->wsUrl&wsfunction=$ws_function&wstoken=$ws_token&";
  }

  /**
   * Generic request to Totara.
   *
   * @param string $method
   *   The request method.
   * @param string $ws_function
   *   The Totara webservice function to call.
   * @param array $params
   *   Parameters for the request.
   * @param string|null $ws_token
   *   Webservice token override. Defaults to NULL, the default of the Totara
   *   Client will be used.
   *
   * @return array
   *   The response body.
   *
   * @throws \Drupal\totara\Exceptions\TotaraRequestFailedException
   * @throws \Drupal\totara\Exceptions\TotaraRequestInvalidResponseException
   */
  protected function request(string $method, string $ws_function, array $params = [], string $ws_token = NULL): array {

    $context = [
      'method' => $method,
      'function' => $ws_function,
      'message' => '',
      'parameters' => json_encode($params),
    ];

    try {

      $options['headers'] = [
        'Content-Type' => $method === 'POST' ? 'application/x-www-form-urlencoded' : 'application/json',
      ];

      if ($params) {
        $options['form_params'] = $params;
      }

      $uri = $this->compileUrl($ws_function, $ws_token);
      $response = $this->httpClient->request($method, $uri, $options);
    }
    catch (GuzzleException $exception) {

      $context['message'] = $exception->getMessage();
      $this->logger->error('{method} to {function} and parameters {parameters}: {message}.', $context);
      throw new TotaraRequestFailedException($exception->getMessage());
    }

    try {

      $raw_body = $response->getBody()->getContents();
      $body = json_decode($raw_body, TRUE);

      if (isset($body['exception'])) {
        throw new \Exception($raw_body);
      }

      return $body ?? [];
    }
    catch (\Exception $exception) {
      $context['message'] = $exception->getMessage();
      $this->logger->error('Invalid response for {method} to {function} and parameters {parameters}: {message}.', $context);
      throw new TotaraRequestInvalidResponseException($exception->getMessage());
    }
  }

}
