<?php

namespace Drupal\totara;

use Drupal\user\UserDataInterface;

/**
 * Defines the Totara User Data service.
 */
interface TotaraUserDataInterface extends UserDataInterface {

  /**
   * The module name to use for the User Data service.
   *
   * @var string
   */
  const MODULE_NAME = 'totara';

  /**
   * The name of the Totara ID value in the User Data service.
   *
   * @var string
   */
  const TOTARA_ID = 'totara_id';

  /**
   * Delete the Totara ID for given User ID.
   *
   * @param int $uid
   *   The User ID.
   */
  public function deleteTotaraId(int $uid);

  /**
   * Get the Totara ID for given User ID.
   *
   * @param int $uid
   *   The User ID.
   *
   * @return int|null
   *   The Totara ID.
   */
  public function getTotaraId(int $uid): ?int;

  /**
   * Set the Totara ID for given User ID.
   *
   * @param int $uid
   *   The User ID.
   * @param int $id
   *   The Totara ID.
   */
  public function setTotaraId(int $uid, int $id): void;

}
