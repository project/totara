<?php

namespace Drupal\totara\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\totara\TotaraClientInterface;

/**
 * Totara config form.
 */
class TotaraConfigForm extends ConfigFormBase {

  /**
   * The Totara config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      TotaraClientInterface::CONFIG_KEY,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'totara_settings_form';
  }

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);

    $config_keys = $this->getEditableConfigNames();
    $this->config = $this->config(reset($config_keys));
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $form[TotaraClientInterface::CONFIG_VALUE_URL] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL'),
      '#description' => $this->t('The URL of the Totara instance.'),
      '#maxlength' => 255,
      '#required' => TRUE,
      '#default_value' => $this->config->get(TotaraClientInterface::CONFIG_VALUE_URL),
    ];
    $form[TotaraClientInterface::CONFIG_VALUE_TOKEN] = [
      '#type' => 'textfield',
      '#title' => $this->t('Webservice token'),
      '#description' => $this->t('The webservice token, as configured for the Service exposing the required webservices.'),
      '#maxlength' => 255,
      '#required' => TRUE,
      '#default_value' => $this->config->get(TotaraClientInterface::CONFIG_VALUE_TOKEN),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $form_state->cleanValues();
    $this->config->setData($form_state->getValues())->save();

    if (\Drupal::service('totara.client')->ping(TRUE)) {
      $this->messenger()->addStatus($this->t('Totara connection OK.'));
    }

    parent::submitForm($form, $form_state);
  }

}
