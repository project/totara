<?php

namespace Drupal\totara\Event;

use Drupal\user\UserInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event that fires when a User is prepared to send to Totara.
 */
class UserDataForTotaraPrepareEvent extends Event {

  const EVENT_NAME = 'totara.user_data_for_totara_prepare_event';

  /**
   * The User.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * The User data for Totara.
   *
   * @var array
   */
  protected $data;

  /**
   * Constructor.
   *
   * @param \Drupal\user\UserInterface $user
   *   The User whos data is begin prepared.
   * @param array $data
   *   The currently prepared data.
   */
  public function __construct(UserInterface $user, array $data) {
    $this->user = $user;
    $this->data = $data;
  }

  /**
   * Get the User.
   *
   * @return \Drupal\user\UserInterface
   *   The User.
   */
  public function getUser(): UserInterface {
    return $this->user;
  }

  /**
   * Get the User data for Totara.
   *
   * @return array
   *   The User data for Totara.
   */
  public function getData(): array {
    return $this->data;
  }

  /**
   * Set the User data for Totara.
   *
   * @param array $data
   *   The User data for Totara.
   */
  public function setData(array $data) {
    $this->data = $data;
  }

}
