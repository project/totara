<?php

namespace Drupal\totara\Event;

/**
 * Event that fires when a new User is prepared to send to Totara.
 *
 * Basically the same event as PrepareUserDataForTotaraEvent, but fired after
 * PrepareUserDataForTotaraEvent where preparing for user create in Totara.
 *
 * This allows to add user datg that only matters when creating a user, such as
 * - a password
 * - the 'createpassword' flag
 * - authentication method
 * - any other field data must have an initial value but not kept in sync from
 *   Drupal to Totara.
 */
class NewUserDataForTotaraPrepareEvent extends UserDataForTotaraPrepareEvent {

  const EVENT_NAME = 'totara.new_user_data_for_totara_prepare_event';

}
