<?php

namespace Drupal\totara;

use Drupal\Core\Database\Connection;
use Drupal\user\UserData;

/**
 * Default implementation of the Totara User Data service.
 */
class TotaraUserData extends UserData implements TotaraUserDataInterface {

  /**
   * Constructs a new user data service.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection to use.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteTotaraId(int $uid) {
    $this->delete(static::MODULE_NAME, $uid, static::TOTARA_ID);
  }

  /**
   * {@inheritdoc}
   */
  public function getTotaraId(int $uid): ?int {

    $id = $this->get(static::MODULE_NAME, $uid, static::TOTARA_ID);
    if ($id === NULL) {
      return NULL;
    }

    return intval($id);
  }

  /**
   * {@inheritdoc}
   */
  public function setTotaraId(int $uid, int $id): void {
    $this->set(static::MODULE_NAME, $uid, static::TOTARA_ID, $id);
  }

}
