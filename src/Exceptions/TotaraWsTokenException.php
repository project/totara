<?php

namespace Drupal\totara\Exceptions;

/**
 * Class TotaraWsTokenException.
 */
class TotaraWsTokenException extends \Exception {}
