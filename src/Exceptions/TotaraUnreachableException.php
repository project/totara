<?php

namespace Drupal\totara\Exceptions;

/**
 * Class TotaraUnreachableException.
 */
class TotaraUnreachableException extends \Exception {}
