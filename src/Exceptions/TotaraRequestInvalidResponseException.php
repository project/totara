<?php

namespace Drupal\totara\Exceptions;

/**
 * Class TotaraRequestInvalidResponseException.
 */
class TotaraRequestInvalidResponseException extends \Exception {}
