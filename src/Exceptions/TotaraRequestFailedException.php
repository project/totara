<?php

namespace Drupal\totara\Exceptions;

/**
 * TotaraRequestFailedException.
 */
class TotaraRequestFailedException extends \Exception {}
