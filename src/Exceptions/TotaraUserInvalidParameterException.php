<?php

namespace Drupal\totara\Exceptions;

/**
 * Class TotaraUserInvalidParameterException.
 */
class TotaraUserInvalidParameterException extends \Exception {}
