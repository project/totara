<?php

namespace Drupal\totara;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Totara Client.
 */
interface TotaraClientInterface extends CacheableDependencyInterface {

  /**
   * The config key of the module settings.
   *
   * @var string
   */
  const CONFIG_KEY = 'totara.settings';

  /**
   * The config key of the token value.
   *
   * @var string
   */
  const CONFIG_VALUE_TOKEN = 'token';

  /**
   * The config key of the url value.
   *
   * @var string
   */
  const CONFIG_VALUE_URL = 'url';

  /*
   * Basic request functions.
   */

  /**
   * Make a GET request.
   *
   * @param string $ws_function
   *   The Totara webservice function to call.
   * @param array $params
   *   Parameters for the request.
   * @param string|null $ws_token
   *   Webservice token override. Defaults to NULL, the default of the Totara
   *   Client will be used.
   *
   * @return array
   *   The response body.
   *
   * @throws \Drupal\totara\Exceptions\TotaraRequestFailedException
   * @throws \Drupal\totara\Exceptions\TotaraRequestInvalidResponseException
   */
  public function get(string $ws_function, array $params = [], string $ws_token = NULL): array;

  /**
   * Make a POST request.
   *
   * @param string $ws_function
   *   The Totara webservice function to call.
   * @param array $params
   *   Parameters for the request.
   * @param string|null $ws_token
   *   Webservice token override. Defaults to NULL, the default of the Totara
   *   Client will be used.
   *
   * @return array
   *   The response body.
   *
   * @throws \Drupal\totara\Exceptions\TotaraRequestFailedException
   * @throws \Drupal\totara\Exceptions\TotaraRequestInvalidResponseException
   */
  public function post(string $ws_function, array $params = [], string $ws_token = NULL): array;

  /*
   * Totara webservice specific request functions.
   */

  /**
   * Create a User in Totara.
   *
   * Uses the Totara webservice 'core_user_create_users'.
   *
   * @param \Drupal\user\UserInterface $user
   *   The User to add.
   *
   * @return \Drupal\user\UserInterface
   *   The User.
   */
  public function addUser(UserInterface $user): UserInterface;

  /**
   * Create Users in Totara.
   *
   * Uses the Totara webservice 'core_user_create_users'.
   *
   * @param \Drupal\user\UserInterface[] $users
   *   Array of Users to add.
   *
   * @return \Drupal\user\UserInterface[]
   *   The updated Users.
   */
  public function addUsers(array $users): array;

  /**
   * Check if User exists in Totara, and create if not.
   *
   * @param \Drupal\user\UserInterface $user
   *   The User to check.
   *
   * @return bool
   *   TRUE if the User exists or has been created.
   */
  public function checkUserExists(UserInterface $user): bool;

  /**
   * Delete a User in Totara.
   *
   * Uses the Totara webservice 'core_user_delete_users'.
   *
   * @param \Drupal\user\UserInterface $user
   *   The User to delete.
   *
   * @return \Drupal\user\UserInterface
   *   The User.
   */
  public function deleteUser(UserInterface $user): UserInterface;

  /**
   * Delete Users in Totara.
   *
   * Uses the Totara webservice 'core_user_delete_users'.
   *
   * @param \Drupal\user\UserInterface[] $users
   *   Array of Users to delete.
   *
   * @return \Drupal\user\UserInterface[]
   *   The Users.
   */
  public function deleteUsers(array $users): array;

  /**
   * Get user data from Totara for User.
   *
   * The user's data will first be requested by its Totara ID.
   * If the User does not have a Totara ID, or no corresponding user in Totara
   * is found, and second lookup by email will be formed.
   *
   * Uses the Totara webservice 'core_user_get_users_by_field'.
   *
   * @param \Drupal\user\UserInterface $user
   *   The User to get the data for.
   *
   * @return array
   *   The User data in Totara.
   */
  public function getUserData(UserInterface $user): array;

  /**
   * Check if Totara is reachable.
   *
   *  Uses the Totara webservice 'core_webservice_get_site_info'.
   *
   * @param bool $show_errors
   *   Whether to show errors, if any, as message. Defaults to FALSE.
   *
   * @return bool
   *   TRUE if Totara does not respond with an error or timeouts.
   */
  public function ping(bool $show_errors = FALSE): bool;

  /**
   * Suspend a User in Totara.
   *
   * Uses the Totara webservice 'core_user_update_users'.
   *
   * @param \Drupal\user\UserInterface $user
   *   The User to suspend.
   */
  public function suspendUser(UserInterface $user): void;

  /**
   * Suspend Users in Totara.
   *
   * Uses the Totara webservice 'core_user_update_users'.
   *
   * @param \Drupal\user\UserInterface[] $users
   *   Array of Users to suspend.
   */
  public function suspendUsers(array $users): void;

  /**
   * Update a User in Totara.
   *
   * Uses the Totara webservice 'core_user_update_users'.
   *
   * @param \Drupal\user\UserInterface $user
   *   The User to update.
   */
  public function updateUser(UserInterface $user): void;

  /**
   * Update Users in Totara.
   *
   * Uses the Totara webservice 'core_user_update_users'.
   *
   * @param \Drupal\user\UserInterface[] $users
   *   Array of Users to update.
   */
  public function updateUsers(array $users): void;

  /*
   * Helper functions.
   */

  /**
   * Get the Totara base URL, or domain.
   *
   * @return string
   *   The base URL.
   */
  public function getBaseUrl(): string;

  /**
   * Get the Totara webservice URL.
   *
   * This does not include webservice function and token.
   *
   * @return string
   *   The webservice URL.
   */
  public function getWsUrl() : string;

  /**
   * Prepare a User's data for Totara.
   *
   * The prepared data is intentionally a 'loose' array, as there ar many
   * optional porameters for the create and update webservice, and many
   * (or most) might not matter in most cases.
   *
   * Also fires the PrepareUserDataForTotaraEvent, so you can whatever you need,
   * including data for custom fields in Totara.
   *
   * @param \Drupal\user\UserInterface $user
   *   The User.
   *
   * @return array
   *   The data.
   *
   * @see \Drupal\totara\Event\UserDataForTotaraPrepareEvent
   */
  public function prepareUserDataForTotara(UserInterface $user): array;

  /**
   * Add a Totara custom field value to the User data for Totara.
   *
   * @param array $data
   *   The  User data for Totara being prepared.
   * @param string $type
   *   The 'type' aka field name of the custom field in Totara.
   * @param string $value
   *   The value of the field.
   */
  public static function addCustomField(array &$data, string $type, string $value): void;

}
